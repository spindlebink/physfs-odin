# PhysFS Odin

1-1 bindings to [PhysFS](https://icculus.org/physfs) in the [Odin programming language](https://odin-lang.org).

## License

BSD 3-clause.
