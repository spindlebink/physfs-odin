package physfs

import "core:c"

when ODIN_OS == .Linux || ODIN_OS == .Darwin {
	foreign import lib "libphysfs.a"
}

@(default_calling_convention="c", link_prefix="PHYSFS_")
foreign lib {
	init :: proc(argv0: cstring) -> c.int ---
	isInit :: proc() -> c.int ---
	deinit :: proc() -> c.int ---
	supportedArchiveTypes :: proc() -> [^]^ArchiveInfo ---
	freeList :: proc(listVar: rawptr) ---
	
	getLastError :: proc() -> cstring ---
	getDirSeparator :: proc() -> cstring ---
	permitSymbolicLinks :: proc(allow: c.int) ---
	getCdRomDirs :: proc() -> [^]cstring ---
	getBaseDir :: proc() -> cstring ---
	getUserDir :: proc() -> cstring ---
	getWriteDir :: proc() -> cstring ---
	setWriteDir :: proc(newDir: cstring) -> c.int ---
	addToSearchPath :: proc(newDir: cstring, appendToPath: c.int) -> c.int ---
	removeFromSearchPath :: proc(oldDir: cstring) -> c.int ---
	getSearchPath :: proc() -> [^]cstring ---
	setSaneConfig :: proc(organization, appName, archiveExt: cstring, includedCdRoms, archivesFirst: c.int) -> c.int ---
	
	mkdir :: proc(dirName: cstring) -> c.int ---
	delete :: proc(filename: cstring) -> c.int ---
	
	getRealDir :: proc(filename: cstring) -> cstring ---
	getPrefDir :: proc(org, app: cstring) -> cstring ---
	enumerateFiles :: proc(dir: cstring) -> [^]cstring ---
	exists :: proc(fname: cstring) -> c.int ---
	isDirectory :: proc(fname: cstring) -> c.int ---
	isSymbolicLink :: proc(fname: cstring) -> c.int ---
	getLastModTime :: proc(fname: cstring) -> sint64 ---
	symbolicLinksPermitted :: proc() -> c.int ---
	getLastErrorCode :: proc() -> ErrorCode ---
	getErrorByCode :: proc(code: ErrorCode) -> cstring ---
	setErrorCode :: proc(code: ErrorCode) ---
	
	openWrite :: proc(filename: cstring) -> ^File ---
	openAppend :: proc(filename: cstring) -> ^File ---
	openRead :: proc(filename: cstring) -> ^File ---
	
	close :: proc(handle: ^File) -> c.int ---
	read :: proc(handle: ^File, buffer: rawptr, objSize, objCount: uint32) -> sint64 ---
	write :: proc(handle: ^File, buffer: rawptr, objSize, objCount: uint32) -> sint64 ---
	eof :: proc(handle: ^File) -> c.int ---
	tell :: proc(handle: ^File) -> sint64 ---
	seek :: proc(handle: ^File, pos: uint64) -> c.int ---
	stat :: proc(fname: cstring, stat: ^Stat) -> c.int ---
	fileLength :: proc(handle: ^File) -> sint64 ---
	setBuffer :: proc(handle: ^File, bufsize: uint64) -> c.int ---
	flush :: proc(handle: ^File) -> c.int ---
	readBytes :: proc(handle: ^File, buffer: rawptr, len: uint64) -> sint64 ---
	writeBytes :: proc(handle: ^File, buffer: rawptr, len: uint64) -> sint64 ---
	
	registerArchiver :: proc(archiver: ^Archiver) -> c.int ---
	deregisterArchiver :: proc(ext: cstring) -> c.int ---
	
	setRoot :: proc(archive, subdir: cstring) -> c.int ---
	setAllocator :: proc(allocator: ^Allocator) -> c.int ---
	getAllocator :: proc() -> ^Allocator ---
	
	mount :: proc(newDir, mountPoint: cstring, appendToPath: c.int) -> c.int ---
	unmount :: proc(oldDir: cstring) -> c.int ---
	mountIo :: proc(io: ^Io, newdir, mountPoint: cstring, appendToPath: c.int) -> c.int ---
	mountMemory :: proc(buf: rawptr, len: uint64, del: proc "c"(rawptr), newDir, mountPoint: cstring, appendToPath: c.int) -> c.int ---
	mountHandle :: proc(file: ^File, newDir, mountPoint: cstring, appendToPath: c.int) -> c.int ---
	
	getMountPoint :: proc(dir: cstring) -> cstring ---
	getCdRomDirsCallback :: proc(c: StringCallback, d: rawptr) ---
	getSearchPathCallback :: proc(c: StringCallback, d: rawptr) ---
	enumerateFilesCallback :: proc(dir: cstring, c: EnumFilesCallback, d: rawptr) ---
	enumerate :: proc(dir: cstring, callback: EnumerateCallback, d: rawptr) -> c.int ---
	
	swapSLE16 :: proc(val: sint16) -> sint16 ---
	swapULE16 :: proc(val: uint16) -> uint16 ---
	swapLE32 :: proc(val: sint32) -> sint32 ---
	swapULE32 :: proc(val: uint32) -> uint32 ---
	swapSLE64 :: proc(val: sint64) -> sint64 ---
	swapULE64 :: proc(val: uint64) -> uint64 ---
	swapSBE16 :: proc(val: sint16) -> sint16 ---
	swapUBE16 :: proc(val: uint16) -> uint16 ---
	swapBE32 :: proc(val: sint32) -> sint32 ---
	swapUBE32 :: proc(val: uint32) -> uint32 ---
	swapSBE64 :: proc(val: sint64) -> sint64 ---
	swapUBE64 :: proc(val: uint64) -> uint64 ---

	readSLE16 :: proc(file: ^File, val: ^sint16) -> c.int ---
	readULE16 :: proc(file: ^File, val: ^uint16) -> c.int ---
	readSLE32 :: proc(file: ^File, val: ^sint32) -> c.int ---
	readULE32 :: proc(file: ^File, val: ^uint32) -> c.int ---
	readSLE64 :: proc(file: ^File, val: ^sint64) -> c.int ---
	readULE64 :: proc(file: ^File, val: ^uint64) -> c.int ---
	readSBE16 :: proc(file: ^File, val: ^sint16) -> c.int ---
	readUBE16 :: proc(file: ^File, val: ^uint16) -> c.int ---
	readSBE32 :: proc(file: ^File, val: ^sint32) -> c.int ---
	readUBE32 :: proc(file: ^File, val: ^uint32) -> c.int ---
	readSBE64 :: proc(file: ^File, val: ^sint64) -> c.int ---
	readUBE64 :: proc(file: ^File, val: ^uint64) -> c.int ---

	writeSLE16 :: proc(file: ^File, val: sint16) -> c.int ---
	writeULE16 :: proc(file: ^File, val: uint16) -> c.int ---
	writeSLE32 :: proc(file: ^File, val: sint32) -> c.int ---
	writeULE32 :: proc(file: ^File, val: uint32) -> c.int ---
	writeSLE64 :: proc(file: ^File, val: sint64) -> c.int ---
	writeULE64 :: proc(file: ^File, val: uint64) -> c.int ---
	writeSBE16 :: proc(file: ^File, val: sint16) -> c.int ---
	writeUBE16 :: proc(file: ^File, val: uint16) -> c.int ---
	writeSBE32 :: proc(file: ^File, val: sint32) -> c.int ---
	writeUBE32 :: proc(file: ^File, val: uint32) -> c.int ---
	writeSBE64 :: proc(file: ^File, val: sint64) -> c.int ---
	writeUBE64 :: proc(file: ^File, val: uint64) -> c.int ---

	utf8FromUcs4 :: proc(src: ^uint32, dst: cstring, len: uint64) ---
	utf8ToUcs4 :: proc(src: cstring, dst: ^uint32, len: uint64) ---
	utf8FromUcs2 :: proc(src: ^uint16, dst: cstring, len: uint64) ---
	utf8ToUcs2 :: proc(src: cstring, dst: ^uint16, len: uint64) ---
	utf8FromLatin1 :: proc(src, dst: cstring, len: uint64) ---
	utf8FromUtf16 :: proc(src: ^uint16, dst: cstring, len: uint64) ---
	utf8ToUtf16 :: proc(src: cstring, dst: ^uint16, len: uint64) ---
	
	caseFold :: proc(from: uint32, to: ^uint32) -> c.int ---
	utf8stricmp :: proc(str1, str2: cstring) -> c.int ---
	utf16stricmp :: proc(str1, str2: cstring) -> c.int ---
	ucs4stricmp :: proc(str1, str2: ^uint32) -> c.int ---
}
