package physfs

import "core:c"

uint8 :: c.uchar
sint8 :: c.char
uint16 :: c.ushort
sint16 :: c.short
uint32 :: c.uint
sint32 :: c.int
uint64 :: c.ulonglong
sint64 :: c.longlong

File :: struct {
	opaque: rawptr,
}

ArchiveInfo :: struct {
	extension, description, author, url: cstring,
	supportsSymlinks: c.int,
}

Version :: struct {
	major, minor, patch: uint8,
}

AndroidInit :: struct {
	jnienv, ctx: rawptr,
}

Allocator :: struct {
	Init: proc "c" () -> c.int,
	Deinit: proc "c" (),
	Malloc: proc "c" (uint64) -> rawptr,
	Realloc: proc "c" (rawptr, uint64) -> rawptr,
	Free: proc "c" (rawptr),
}

StringCallback :: proc "c" (rawptr, cstring)
EnumFilesCallback :: proc "c" (rawptr, cstring, cstring)

EnumerateCallbackResult :: enum c.int {
	ERROR = -1,
	STOP = 0,
	OK = 1,
}

EnumerateCallback :: proc "c" (rawptr, cstring, cstring) -> EnumerateCallbackResult

FileType :: enum c.int {
	REGULAR,
	DIRECTORY,
	SYMLINK,
	OTHER,
}

Stat :: struct {
	filesize, modtime, createtime, accesstime: sint64,
	filetype: FileType,
	readonly: c.int,
}

Io :: struct {
	version: uint32,
	opaque: rawptr,
	read: proc "c" (^Io, rawptr, uint64) -> sint64,
	write: proc "c" (^Io, rawptr, uint64) -> sint64,
	seek: proc "c" (^Io, uint64) -> c.int,
	tell: proc "c" (^Io) -> sint64,
	length: proc "c" (^Io) -> sint64,
	duplicate: proc "c" (^Io) -> ^Io,
	flush: proc "c" (^Io) -> c.int,
	destroy: proc "c" (^Io),
}

ErrorCode :: enum c.int {
	OK,
	OTHER_ERROR,
	OUT_OF_MEMORY,
	NOT_INITIALIZED,
	IS_INITIALIZED,
	ARGV0_IS_NULL,
	UNSUPPORTED,
	PAST_EOF,
	FILES_STILL_OPEN,
	INVALID_ARGUMENT,
	NOT_MOUNTED,
	NOT_FOUND,
	SYMLINK_FORBIDDEN,
	NO_WRITE_DIR,
	OPEN_FOR_READING,
	OPEN_FOR_WRITING,
	NOT_A_FILE,
	READ_ONLY,
	CORRUPT,
	SYMLINK_LOOP,
	IO,
	PERMISSION,
	NO_SPACE,
	BAD_FILENAME,
	BUSY,
	DIR_NOT_EMPTY,
	OS_ERROR,
	DUPLICATE,
	BAD_PASSWORD,
	APP_CALLBACK,
}

Archiver :: struct {
	version: uint32,
	info: ArchiveInfo,
	openArchive: proc "c" (^Io, cstring, c.int, ^c.int) -> rawptr,
	enumerate: proc "c" (rawptr, cstring, EnumerateCallback, cstring, rawptr) -> EnumerateCallbackResult,
	openRead: proc "c" (rawptr, cstring) -> ^Io,
	openWrite: proc "c" (rawptr, cstring) -> ^Io,
	openAppend: proc "c" (rawptr, cstring) -> ^Io,
	remove: proc "c" (rawptr, cstring) -> c.int,
	mkdir: proc "c" (rawptr, cstring) -> c.int,
	stat: proc "c" (rawptr, cstring, ^Stat) -> c.int,
	closeArchive: proc "c" (rawptr),
}
